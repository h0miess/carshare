package ru.crc.carshare.model;

public enum PaymentPeriodEnum {
    MONTH,
    WEEK,
    DAY,
    HOUR
}
