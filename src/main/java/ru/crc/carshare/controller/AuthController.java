package ru.crc.carshare.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import ru.crc.carshare.controller.api.AuthApi;
import ru.crc.carshare.dto.LoginRequest;
import ru.crc.carshare.dto.LoginResponse;
import ru.crc.carshare.dto.UserDto;
import ru.crc.carshare.mapper.UserMapper;
import ru.crc.carshare.model.UserData;
import ru.crc.carshare.service.api.AuthService;

@RequiredArgsConstructor
@RestController
public class AuthController implements AuthApi {

    private final AuthService authService;

    @Override
    public ResponseEntity<UserDto> register(UserDto userDto) {
        UserData userData = authService.register(UserMapper.mapFromDto(userDto));
        return new ResponseEntity<>(UserMapper.mapFromData(userData), HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<LoginResponse> login(LoginRequest request) {
        UserData userData = UserData.builder()
                .login(request.login())
                .password(request.password())
                .build();

        LoginResponse response = LoginResponse.builder()
                .token(authService.login(userData))
                .build();
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
