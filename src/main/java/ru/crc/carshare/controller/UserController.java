package ru.crc.carshare.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;
import ru.crc.carshare.controller.api.UserApi;
import ru.crc.carshare.dto.UserDto;
import ru.crc.carshare.mapper.UserMapper;
import ru.crc.carshare.service.api.UserService;

@RequiredArgsConstructor
@RestController
@CrossOrigin
public class UserController implements UserApi {
    private final UserService userService;

    @Override
    public UserDto getById(Long id) {
        return UserMapper.mapFromData(userService.getById(id));
    }
}
