package ru.crc.carshare.controller.api;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.crc.carshare.dto.LoginRequest;
import ru.crc.carshare.dto.LoginResponse;
import ru.crc.carshare.dto.UserDto;

@RequestMapping("/api/v1/auth")
@CrossOrigin
public interface AuthApi {

    @PostMapping("/register")
    ResponseEntity<UserDto> register(@RequestBody UserDto userDto);

    @PostMapping("/login")
    ResponseEntity<LoginResponse> login(@RequestBody LoginRequest request);
}
