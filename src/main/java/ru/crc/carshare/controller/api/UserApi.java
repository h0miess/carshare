package ru.crc.carshare.controller.api;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.crc.carshare.dto.UserDto;

@RequestMapping("/api/v1/user")
@CrossOrigin
public interface UserApi {

    @GetMapping("{id}")
    UserDto getById(@PathVariable("id") Long id);
}
