package ru.crc.carshare.controller.api;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.crc.carshare.dto.AdDto;
import ru.crc.carshare.dto.AdResponse;

import java.util.List;

@RequestMapping("/api/v1/ad")
@CrossOrigin
public interface AdApi {

    @PostMapping()
    ResponseEntity<Void> create(AdDto adDto);

    @GetMapping()
    ResponseEntity<List<AdResponse>> getAll();

    @GetMapping("{id}")
    ResponseEntity<AdResponse> getById(@PathVariable("id") Long id);

    @DeleteMapping("{id}")
    ResponseEntity<Void> deleteById(Long id);

}
