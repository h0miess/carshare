package ru.crc.carshare.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;
import ru.crc.carshare.controller.api.AdApi;
import ru.crc.carshare.dto.AdDto;
import ru.crc.carshare.dto.AdResponse;
import ru.crc.carshare.mapper.AdMapper;
import ru.crc.carshare.service.api.AdService;

import java.util.List;

@RestController
@CrossOrigin
@RequiredArgsConstructor
public class AdController implements AdApi {

    private final AdService adService;

    @Override
    public ResponseEntity<Void> create(AdDto adDto) {
        adService.create(AdMapper.mapFromAdDto(adDto));
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<List<AdResponse>> getAll() {
        return ResponseEntity.ok(adService.getAll().stream()
                .map(AdMapper::mapFromDataToResponse)
                .toList()
        );
    }

    @Override
    public ResponseEntity<AdResponse> getById(Long id) {
        return ResponseEntity.ok(AdMapper.mapFromDataToResponse(adService.getById(id)));
    }

    @Override
    public ResponseEntity<Void> deleteById(Long id) {
        adService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
