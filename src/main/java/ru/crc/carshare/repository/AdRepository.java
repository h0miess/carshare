package ru.crc.carshare.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.crc.carshare.model.Ad;

@Repository
public interface AdRepository extends JpaRepository<Ad, Long> {
}
