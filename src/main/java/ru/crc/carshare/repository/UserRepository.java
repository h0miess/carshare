package ru.crc.carshare.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.crc.carshare.model.UserData;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserData, Long> {
    Optional<UserData> findByLogin(String login);

    boolean existsByLoginOrPhone(String login, String phone);
}
