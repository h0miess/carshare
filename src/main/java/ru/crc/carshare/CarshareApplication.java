package ru.crc.carshare;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EntityScan(basePackages = {"ru.crc.carshare.model"})
@EnableJpaRepositories(basePackages = {"ru.crc.carshare.repository"})
public class CarshareApplication {

	public static void main(String[] args) {
		SpringApplication.run(CarshareApplication.class, args);
	}

}
