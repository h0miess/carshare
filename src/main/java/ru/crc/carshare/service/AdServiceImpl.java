package ru.crc.carshare.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.crc.carshare.exception.AdNotFoundException;
import ru.crc.carshare.model.Ad;
import ru.crc.carshare.repository.AdRepository;
import ru.crc.carshare.service.api.AdService;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class AdServiceImpl implements AdService {

    private final AdRepository adRepository;

    @Override
    public void create(Ad ad) {
        adRepository.save(ad);
    }

    @Override
    public Ad getById(Long id) {
        return adRepository.findById(id).orElseThrow(() -> new AdNotFoundException(id));
    }

    @Override
    public List<Ad> getAll() {
        return new ArrayList<>(adRepository.findAll());
    }

    @Override
    public void deleteById(Long id) {
        adRepository.deleteById(id);
    }
}
