package ru.crc.carshare.service.api;

import ru.crc.carshare.model.UserData;

public interface AuthService {
    UserData register(UserData user);

    String login(UserData userData);
}
