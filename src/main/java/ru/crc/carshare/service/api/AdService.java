package ru.crc.carshare.service.api;

import ru.crc.carshare.model.Ad;

import java.util.List;

public interface AdService {

    void create(Ad ad);

    Ad getById(Long id);

    List<Ad> getAll();

    void deleteById(Long id);
}
