package ru.crc.carshare.service.api;

import ru.crc.carshare.model.UserData;

public interface UserService {
    UserData getById(Long id);
}
