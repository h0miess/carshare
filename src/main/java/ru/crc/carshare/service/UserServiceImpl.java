package ru.crc.carshare.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.crc.carshare.exception.UserNotFoundException;
import ru.crc.carshare.model.UserData;
import ru.crc.carshare.repository.UserRepository;
import ru.crc.carshare.service.api.UserService;

@RequiredArgsConstructor
@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    @Override
    public UserData getById(Long id) {
        return userRepository.findById(id).orElseThrow(() -> new UserNotFoundException(id));
    }
}
