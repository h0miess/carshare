package ru.crc.carshare.service;

import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.crc.carshare.exception.UserAlreadyExistsException;
import ru.crc.carshare.exception.UserNotFoundException;
import ru.crc.carshare.model.UserData;
import ru.crc.carshare.repository.UserRepository;
import ru.crc.carshare.security.JwtService;
import ru.crc.carshare.security.UserDetailsImpl;
import ru.crc.carshare.service.api.AuthService;

@RequiredArgsConstructor
@Service
public class AuthServiceImpl implements AuthService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;

    @Override
    public UserData register(UserData user) {
        if (userRepository.existsByLoginOrPhone(user.getLogin(), user.getPhone())) {
            throw new UserAlreadyExistsException();
        }

        //TODO validate login, name, email, username and phone

        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userRepository.save(user);
    }

    @Override
    public String login(UserData request) {
        UserData user = userRepository.findByLogin(request.getLogin())
                .orElseThrow(() -> new UserNotFoundException("User with username " + request.getLogin() + " not found"));

        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        request.getLogin(),
                        request.getPassword()
                )
        );

        return jwtService.generateToken(new UserDetailsImpl(user));
    }
}
