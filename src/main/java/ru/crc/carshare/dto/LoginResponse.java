package ru.crc.carshare.dto;

import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Builder;

@JsonTypeName("loginResponse")
@Builder
public record LoginResponse(String token) {}
