package ru.crc.carshare.dto;

import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("loginRequest")
public record LoginRequest(String login, String password) {}