package ru.crc.carshare.dto;

import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Builder;

@JsonTypeName("user")
@Builder
public record UserDto(
        String login,
        String password,
        String name,
        String surname,
        String email,
        String phone) {
}
