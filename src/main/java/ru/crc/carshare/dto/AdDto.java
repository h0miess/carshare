package ru.crc.carshare.dto;

import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Builder;
import ru.crc.carshare.model.PaymentPeriodEnum;

@JsonTypeName("AdRequest")
@Builder
public record AdDto (
        String title,
        Integer price,
        PaymentPeriodEnum paymentPeriod,
        String description)
{}