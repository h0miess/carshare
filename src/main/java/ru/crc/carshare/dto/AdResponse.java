package ru.crc.carshare.dto;

import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Builder;
import ru.crc.carshare.model.PaymentPeriodEnum;

@JsonTypeName("AdResponse")
@Builder
public record AdResponse(
        Long id,
        String title,
        Integer price,
        PaymentPeriodEnum paymentPeriod,
        String description,
        UserDto author
) {}
