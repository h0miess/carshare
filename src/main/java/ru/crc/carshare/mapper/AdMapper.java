package ru.crc.carshare.mapper;

import lombok.experimental.UtilityClass;
import ru.crc.carshare.dto.AdDto;
import ru.crc.carshare.dto.AdResponse;
import ru.crc.carshare.model.Ad;

@UtilityClass
public class AdMapper {

    public Ad mapFromAdDto(AdDto adDto) {
        return Ad.builder()
                .title(adDto.title())
                .price(adDto.price())
                .paymentPeriod(adDto.paymentPeriod())
                .description(adDto.description())
                .build();
    }

    public AdResponse mapFromDataToResponse(Ad ad) {
        return AdResponse.builder()
                .id(ad.getId())
                .title(ad.getTitle())
                .price(ad.getPrice())
                .paymentPeriod(ad.getPaymentPeriod())
                .description(ad.getDescription())
                .author(UserMapper.mapFromData(ad.getAuthor()))
                .build();
    }
}
