package ru.crc.carshare.mapper;

import lombok.experimental.UtilityClass;
import ru.crc.carshare.dto.UserDto;
import ru.crc.carshare.model.UserData;

@UtilityClass
public class UserMapper{
    public UserData mapFromDto(UserDto userDto) {
        return UserData.builder()
                .login(userDto.login())
                .password(userDto.password())
                .name(userDto.name())
                .surname(userDto.surname())
                .email(userDto.email())
                .phone(userDto.phone())
                .build();
    }

    public UserDto mapFromData(UserData user) {
        return UserDto.builder()
                .login(user.getLogin())
                .password(user.getPassword())
                .name(user.getName())
                .surname(user.getSurname())
                .email(user.getEmail())
                .phone(user.getPhone())
                .build();
    }
}
