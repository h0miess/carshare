CREATE TABLE if not exists usr(id serial primary key,
    email varchar(255) not null,
    login varchar(255) not null,
    password varchar(255) not null,
    name varchar(255) not null,
    surname varchar(255) not null,
    phone varchar(255) not null
);

create table if not exists ads(
    id serial primary key,
    title varchar(255) not null,
    description text null,
    price integer not null,
    payment_period varchar(255) not null,
    author_id integer not null,
    created_at varchar(255) not null
);