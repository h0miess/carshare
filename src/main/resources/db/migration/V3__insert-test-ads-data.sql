insert into ads (created_at, description, payment_period, price, title, author_id)
values ('25.04.2024 21:13', 'some description', 'DAY', 1000, 'Honda Civic', 1);

insert into ads (created_at, description, payment_period, price, title, author_id)
values ('26.04.2024 21:13', 'biiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiig description', 'MONTH', 45000, 'BMW 3', 2);

insert into ads (created_at, description, payment_period, price, title, author_id)
values ('24.04.2024 13:15', 'description', 'HOUR', 200, 'Kia Rio', 3);

insert into ads (created_at, description, payment_period, price, title, author_id)
values ('20.03.2024 00:55', 'very good car, please rent', 'DAY', 830, 'Daewoo Matiz', 3);

insert into ads (created_at, description, payment_period, price, title, author_id)
values ('10.11.2023 16:30', 'it is perfect car for delivery work ', 'WEEK', 5600, 'Lada 2114', 2);
